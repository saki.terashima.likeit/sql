﻿create database item_category default character set utf8;

use item;

CREATE TABLE item_category(category_id int AUTO_INCREMENT PRIMARY KEY ,
						   category_name varchar(256) NOT NULL);
