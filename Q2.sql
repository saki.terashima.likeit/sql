﻿create database item default character set utf8;

use item;

CREATE TABLE item(item_id int AUTO_INCREMENT PRIMARY KEY ,
                  item_name varchar(256) NOT NULL,
				  item_price int NOT NULL,
				  category_id int);
