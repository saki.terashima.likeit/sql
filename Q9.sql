﻿use item_category;

select
   category_name,  sum(item_price) as total_price 

from 
  item_category

inner join
  item

on
  item.category_id=item_category.category_id

group by
  category_name
order by total_price desc;
