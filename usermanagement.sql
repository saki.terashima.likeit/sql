CREATE DATABASE usermanagement DEFAULT CHARACTER SET utf8;

use usermanagement;

create table user(id SERIAL PRIMARY KEY AUTO_INCREMENT,
                  login_id varchar(255) UNIQUE NOT NULL,
				  name varchar(255) NOT NULL,
				  birth_date DATE NOT NULL,
				  password varchar(255) NOT NULL,
				  create_date DATETIME NOT NULL,
				  update_date DATETIME NOT NULL);
				  

insert into user(login_id,name)
            values('admin','�Ǘ���');

insert into user(login_id,name)
            values('aaa','�c��');

insert into user(login_id,name)
            values('bbb','����');

insert into user(login_id,name)
            values('ccc','���');

DROP table user;

select * from user;
